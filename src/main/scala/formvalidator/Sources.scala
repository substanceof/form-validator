package formvalidator

trait Sources {

  case class ProvideSource[K](ps: ParamSource[K]) extends ParamSourceProvider[K]
  
  object MapSource {
    def apply[K](map: Map[K, String]) =
      ProvideSource(new MapSource(map))
  }

  class MapSource[K](map: Map[K, String]) extends ParamSource[K] {
    def get(k: K) = map.get(k)
  }
}
