package formvalidator

import org.apache.commons.validator.routines.EmailValidator

import scala.util.matching.Regex

trait Constraints {

  object MinLength {
    case class TooShort(actual: Int, min: Int) extends ConstraintFailure
  }

  class MinLength(min: Int) extends Constraint[String, TextField] {
    import MinLength._

    def valid(in: String) =
      if (in.lengthCompare(min) < 0) TooShort(in.length, min)
      else ConstraintSuccess
  }

  def minLn(min: Int) = new MinLength(min)

  object MaxLength {
    case class TooLong(actual: Int, max: Int) extends ConstraintFailure
  }

  class MaxLength(max: Int) extends Constraint[String, TextField] {
    import MaxLength._

    def valid(in: String) =
      if (in.lengthCompare(max) > 0) TooLong(in.length, max)
      else ConstraintSuccess
  }

  def maxLn(max: Int) = new MaxLength(max)

  object LengthBetween {
    case class LengthNotInBounds(actual: Int, min: Int, max: Int)
        extends ConstraintFailure
  }

  class LengthBetween(min: Int, max: Int)
      extends Constraint[String, TextField] {
    import LengthBetween._

    def valid(in: String) =
      if (in.lengthCompare(min) < 0 || in.lengthCompare(max) > 0)
        LengthNotInBounds(in.length, min, max)
      else ConstraintSuccess
  }

  def lnBetween(min: Int, max: Int) = new LengthBetween(min, max)

  object NonEmpty {
    val re = "[\\s]*".r.pattern
    case object IsEmpty extends ConstraintFailure
  }

  class NonEmpty(allowWS: Boolean) extends Constraint[String, TextField] {
    import NonEmpty._

    def valid(in: String) = {
      val empty =
        if (allowWS) in.isEmpty
        else re.matcher(in).matches

      if (empty) IsEmpty
      else ConstraintSuccess
    }
  }

  def nonEmpty        = new NonEmpty(allowWS = false)
  def nonEmptyAllowWS = new NonEmpty(allowWS = true)

  object Min {
    case class LessThanMin(actual: Int, min: Int) extends ConstraintFailure
  }

  class Min(min: Int) extends Constraint[Int, NumberField] {
    import Min._

    def valid(in: Int) =
      if (in < min) LessThanMin(in, min)
      else ConstraintSuccess
  }

  def min(m: Int) = new Min(m)
  def nonNegative = new Min(0)
  def positive    = new Min(1)

  object Max {
    case class MoreThanMax(actual: Int, max: Int) extends ConstraintFailure
  }

  class Max(max: Int) extends Constraint[Int, NumberField] {
    import Max._

    def valid(in: Int) =
      if (in > max) MoreThanMax(in, max)
      else ConstraintSuccess
  }

  def max(max: Int) = new Max(max)
  def nonPositive   = new Max(0)
  def negative      = new Max(-1)

  object Between {
    case class NotInBounds(actual: Int, min: Int, max: Int)
        extends ConstraintFailure
  }

  class Between(min: Int, max: Int) extends Constraint[Int, NumberField] {
    import Between._

    def valid(in: Int) =
      if (in < min || in > max) NotInBounds(in, min, max)
      else ConstraintSuccess
  }

  def numBetween(min: Int, max: Int) = new Between(min, max)

  object OneOf {
    case class NotInSet[T](in: T, set: Set[T]) extends ConstraintFailure
  }

  class OneOf[T, F <: Field[T]](val set: Set[T]) extends Constraint[T, F] {
    import OneOf._

    def valid(in: T) =
      if (set.contains(in)) ConstraintSuccess
      else NotInSet(in, set)
  }

  def oneOf[T](s: T*)     = new OneOf(s.toSet)
  def oneOf[T](s: Set[T]) = new OneOf(s)

  object NotOneOf {
    case class IsInSet[T](in: T, set: Set[T]) extends ConstraintFailure
  }

  class NotOneOf[T, F <: Field[T]](val set: Set[T]) extends Constraint[T, F] {
    import NotOneOf._

    def valid(in: T) =
      if (set.contains(in)) IsInSet(in, set)
      else ConstraintSuccess
  }

  def notOneOf[T](s: T*)     = new NotOneOf(s.toSet)
  def notOneOf[T](s: Set[T]) = new NotOneOf(s)

  object Bool {
    case class NotValidated[T](in: T) extends ConstraintFailure
  }

  class Bool[T, F <: Field[T]](cond: T => Boolean) extends Constraint[T, F] {
    import Bool._

    def valid(in: T) =
      if (cond(in)) ConstraintSuccess
      else NotValidated(in)
  }

  def is[T](f: T => Boolean)    = new Bool(f)
  def isNot[T](f: T => Boolean) = new Bool((a: T) => !f(a))
  def condition[T, F <: Field[T]](f: T => Boolean) =
    new Bool[T, F](f) // standalone explicitly typed condition

  object IsEmail {
    val vlEmail = EmailValidator.getInstance()
    case class NotAValidEmail(in: String) extends ConstraintFailure
  }

  class IsEmail extends Constraint[String, TextField] {
    import IsEmail._

    def valid(in: String) =
      if (vlEmail.isValid(in)) ConstraintSuccess
      else NotAValidEmail(in)
  }

  def isEmail = new IsEmail

  object Pattern {
    case class NotMatched(in: String, pattern: String) extends ConstraintFailure
  }

  class Pattern(val pat: Regex) extends Constraint[String, TextField] {
    import Pattern._
    val c      = pat.pattern
    val rawPat = c.pattern

    def valid(in: String) =
      if (c.matcher(in).matches) ConstraintSuccess
      else NotMatched(in, rawPat)
  }

  def pattern(pat: Regex)       = new Pattern(pat)
  def pattern(pat: String)      = new Pattern(pat.r)
  def anyPattern(pats: String*) = new Pattern(pats.mkString("(", "|", ")").r)

  object StartsWith {
    case class NotStartsWith(in: String, start: Seq[String])
        extends ConstraintFailure
  }

  class StartsWith(s: Seq[String]) extends Constraint[String, TextField] {
    import StartsWith._

    def valid(in: String): ConstraintResult = {
      for (p <- s) if (in.startsWith(p)) return ConstraintSuccess
      NotStartsWith(in, s)
    }
  }

  def startsWith(s: String*) = new StartsWith(s)

}
