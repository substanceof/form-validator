package formvalidator

import scala.annotation.tailrec
import scala.collection.mutable
import scala.language.higherKinds
import scala.util.{Either, Failure, Left, Right, Success, Try}

trait FormValidation {

  case class ExtractionException(f: ExtractionFailure) extends Exception

  case class ExtractionFailure(name: String, label: String, f: ValidationFailure)

  sealed trait ValidationResult[+T]

  case class ValidationSuccess[T](v: T) extends ValidationResult[T]

  sealed trait ValidationFailure extends ValidationResult[Nothing]

  sealed trait ConstraintResult

  case object ConstraintSuccess extends ConstraintResult

  trait ConstraintFailure extends ConstraintResult with ValidationFailure {
    lazy val path = "cst:" + getClass.getName
  }

  sealed trait FieldResult[+T]

  case class FieldSuccess[T](v: T) extends FieldResult[T]

  trait FieldFailure extends FieldResult[Nothing] with ValidationFailure {
    lazy val path = "field:" + getClass.getName
  }

  case object MissingField extends ValidationFailure

  trait Constraint[T, +F <: Field[T]] {
    def valid(in: T): ConstraintResult
  }

  type ConstraintFor[T] = Constraint[T, Field[T]]

  sealed trait FailureReporter {
    private[formvalidator] def present: Boolean
    private[formvalidator] def add(f: ExtractionFailure): Unit
  }

  private[formvalidator] object VoidReporter extends FailureReporter {
    private[formvalidator] val present                         = false
    private[formvalidator] def add(f: ExtractionFailure): Unit = ()
  }

  private[formvalidator] object VoidRepP extends ReporterProvider {
    val rep = VoidReporter
  }

  sealed class Reporter private[formvalidator] () extends FailureReporter {
    private[formvalidator] val buf = new mutable.ArrayBuffer[ExtractionFailure]

    private[formvalidator] val present                         = true
    private[formvalidator] def add(f: ExtractionFailure): Unit = buf += f
  }

  trait ParamSource[K] {
    def get(k: K): Option[String]
  }

  trait ParamSourceProvider[K] {
    def ps: ParamSource[K]
  }

  trait ReporterProvider {
    def rep: FailureReporter
  }

  trait Src[K] {
    def field: K
    def label: String
    def get: Option[String]

    def fieldStr: String = field.toString

    protected def filterEmpty(in: Option[String], needed: Boolean): Option[String] =
      if (needed) in.filter(_.nonEmpty)
      else in

    def <<[T, F[A] <: Field[A]](field: F[T]): FBox[K, T, F] =
      new FBox(field, this)
  }

  class SrcMap[K](val field: K,
                  val label: String,
                  src: ParamSource[K],
                  emptyIsAbsent: Boolean = false)
      extends Src[K] {
    def get: Option[String] =
      filterEmpty(src.get(field), emptyIsAbsent)
  }

  class SrcVal(val field: String, val label: String, v: String, emptyIsAbsent: Boolean = false) extends Src[String] {
    def get: Option[String] =
      filterEmpty(Some(v), emptyIsAbsent)
  }

  def field[K](k: K, label: String = "")(implicit ps: ParamSourceProvider[K]) =
    new SrcMap(k, label, ps.ps)

  def fieldNE[K](k: K, label: String = "")(
      implicit ps: ParamSourceProvider[K]) =
    new SrcMap(k, label, ps.ps, emptyIsAbsent = true)

  def value(v: String, name: String = "", label: String = "") =
    new SrcVal(name, label, v)

  def valueNE(v: String, name: String = "", label: String = "") =
    new SrcVal(name, label, v, emptyIsAbsent = true)

  implicit class StringField(name: String) {
    def <<[T, F[A] <: Field[A]](fld: F[T])(implicit ps: ParamSourceProvider[String]): FBox[String, T, F] =
      field(name)(ps) << fld
  }

  abstract class Field[T](cst: Seq[ConstraintFor[T]]) {
    def defaultValue: T
    def parseValue(in: String): FieldResult[T]

    @tailrec private def reduceConstraints(c: Seq[ConstraintFor[T]],
                                           in: T): ConstraintResult =
      if (c.isEmpty) ConstraintSuccess
      else
        c.head.valid(in) match {
          case ConstraintSuccess    => reduceConstraints(c.tail, in)
          case f: ConstraintFailure => f
        }

    def checkValue(in: String): ValidationResult[T] = parseValue(in) match {
      case f: FieldFailure => f
      case FieldSuccess(v) =>
        reduceConstraints(cst, v) match {
          case f: ConstraintFailure => f
          case ConstraintSuccess    => ValidationSuccess(v)
        }
    }
  }

  class FBox[K, T, +F[A] <: Field[A]](val field: F[T], val src: Src[K]) {
    def <<[C[_]](ext: Extractor[T, C]): C[T] = ext.get(this)
  }

  type Id[T]  = T
  type Box[T] = FBox[_, T, Field]

  abstract class Extractor[T, C[_]](repP: ReporterProvider) {

    protected def checkedValue(box: Box[T]): Option[ValidationResult[T]] =
      box.src.get.map(box.field.checkValue)

    protected def fail(box: Box[T], f: ValidationFailure) =
      ExtractionFailure(box.src.fieldStr, box.src.label, f)

    protected def multi[R](f: ExtractionFailure, default: => R): R = {
      val rep = repP.rep
      if (!rep.present) throw ExtractionException(f)
      else {
        rep.add(f)
        default
      }
    }

    def get(box: Box[T]): C[T]
  }

  class ExtractorOpt[T](implicit rep: ReporterProvider)
      extends Extractor[T, Option](rep) {

    def get(box: Box[T]): Option[T] =
      checkedValue(box) match {
        case Some(f: ValidationFailure) => multi(fail(box, f), None)
        case Some(ValidationSuccess(v)) => Some(v)
        case None                       => None
      }
  }

  class ExtractorDef[T](default: => T)(implicit rep: ReporterProvider)
      extends Extractor[T, Id](rep) {

    def get(box: Box[T]): T =
      checkedValue(box)
        .map {
          case f: ValidationFailure => multi(fail(box, f), default)
          case ValidationSuccess(v) => v
        }
        .getOrElse(default)
  }

  class ExtractorReq[T](implicit rep: ReporterProvider)
      extends Extractor[T, Id](rep) {

    def get(box: Box[T]): T = {
      def default = box.field.defaultValue
      checkedValue(box) match {
        case Some(f: ValidationFailure) => multi(fail(box, f), default)
        case Some(ValidationSuccess(v)) => v
        case None                       => multi(fail(box, MissingField), default)
      }
    }
  }

  def req[T](implicit rep: ReporterProvider) = new ExtractorReq[T]
  def opt[T](implicit rep: ReporterProvider) = new ExtractorOpt[T]
  def default[T](d: => T)(implicit rep: ReporterProvider) =
    new ExtractorDef[T](d)

  def reqNow[T] = req[T](VoidRepP)
  def optNow[T] = opt[T](VoidRepP)
  def defaultNow[T](d: => T) =
    default[T](d)(VoidRepP)

}
