package formvalidator

import scala.util.Try

object FailureMapper {
  val empty: FailureMapper[Nothing] =
    FailureMapper(PartialFunction.empty)
}

case class FailureMapper[T](pf: PartialFunction[EF, T])

trait ErrorExtractors {

  object Field {
    def unapply(a: ExtractionFailure): Option[(String, ValidationFailure)] =
      Some((a.name, a.f))
  }

  object Label {
    def unapply(a: ExtractionFailure): Option[(String, ValidationFailure)] =
      Some((a.label, a.f))
  }

  object Both {
    def unapply(
        a: ExtractionFailure): Option[((String, String), ValidationFailure)] =
      Some(((a.name, a.label), a.f))
  }

}

trait Results {
  type EF      = ExtractionFailure
  type MFun[T] = EF => T
  type SFun[T] = Seq[EF] => Seq[T]

  implicit class MFunMapAll[T](fun: MFun[T]) {
    def mapEach: SFun[T] = _.map(fun)
  }

  def mapFailure[T](implicit mp: FailureMapper[T]): FailureMapFun[T, T, T] =
    new FailureMapFun(mp, a => a, (_, a) => a)

  def mapFailureNow[R](f: MFun[R]): MFun[R] = f

  def result[R](f: ReporterProvider => R): FormResult[R] = {
    val rep = new Reporter
    val res = f(new JustReporter(rep))
    new FormResult(res, rep)
  }

  def fromMap[K, R](map: Map[K, String])(
      f: SourceWithReporter[K] => R): FormResult[R] = {
    val src = new MapSource(map)
    val rep = new Reporter
    val imp = new SourceWithReporter(src, rep)
    val res = f(imp)
    new FormResult(res, rep)
  }
}

class FailureMapFun[M, P, O](mapper: FailureMapper[M],
                             preMapF: M => P,
                             postMapF: (EF, P) => O) {

  def preMap[R](pm: M => R): FailureMapFun[M, R, R] =
    new FailureMapFun(mapper, pm, (_, a) => a)

  def postMap[R](pm: (EF, P) => R): FailureMapFun[M, P, R] =
    new FailureMapFun(mapper, preMapF, pm)

  def withDefault(default: => P): MFun[O] =
    withFallback(_ => default)

  def withFallback(fb: MFun[P]): MFun[O] =
    ef => postMapF(ef, mapper.pf.andThen(preMapF).applyOrElse(ef, fb))
}

case class FormErrors(s: Seq[ExtractionFailure]) extends Throwable

sealed class FormResult[T] private[formvalidator] (res: T, rep: Reporter) {

  val errors    = rep.buf.toList
  val hasErrors = errors.nonEmpty

  def getUnsafe: T =
    if (hasErrors) throw FormErrors(errors)
    else res

  def get[R](onSuccess: T => R, onError: Seq[EF] => R): R =
    if (hasErrors) onError(errors)
    else onSuccess(res)

  def getOr(onError: Seq[EF] => T): T =
    get(a => a, onError)

  def toOption: Option[T] =
    if (hasErrors) None
    else Some(res)

  def toEither: Either[Seq[EF], T] =
    if (hasErrors) Left(errors)
    else Right(res)

  def toTry: Try[T] =
    if (hasErrors) util.Failure(FormErrors(errors))
    else util.Success(res)
}

sealed class SourceWithReporter[K] private[formvalidator] (
    val ps: ParamSource[K],
    val rep: FailureReporter
) extends ReporterProvider
    with ParamSourceProvider[K]

sealed class JustReporter private[formvalidator] (
    val rep: FailureReporter
) extends ReporterProvider
