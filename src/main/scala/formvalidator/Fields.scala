package formvalidator
import scala.util._

trait Fields {

  // === fields ===

  type TextC = Constraint[String, TextField]

  class TextField(cs: Seq[TextC]) extends Field[String](cs) {
    val defaultValue = ""
    def parseValue(in: String) = FieldSuccess(in)
  }

  def text(cs: TextC*): TextField = new TextField(cs)
  def text: TextField             = text()

  object NumberField {
    class NumberFieldError extends FieldFailure
    case object ParseError extends NumberFieldError
  }

  type NumC = Constraint[Int, NumberField]

  class NumberField(cs: Seq[NumC]) extends Field[Int](cs) {
    import NumberField._

    val defaultValue = 0
    def parseValue(in: String) = Try(in.toInt) match {
      case Success(v) => FieldSuccess(v)
      case Failure(_) => ParseError
    }
  }

  def number(cs: NumC*): NumberField = new NumberField(cs)
  def number: NumberField            = number()
}
