package object formvalidator
    extends FormValidation
    with Results
    with ErrorExtractors
    with Constraints
    with Sources
    with Fields {

  def formValidatorInstance = this
}

