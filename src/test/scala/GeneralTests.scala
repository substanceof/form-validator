import org.scalatest.{FlatSpec, Matchers}

import scala.util.Try

class GeneralTests extends FlatSpec with Matchers {

  import formvalidator._
  import formvalidator.{value => v}

  val va = formValidatorInstance

  type MAP = Map[String, String]

  case class A(
      strA: String,
      sbtB: String,
      strC: Option[String]
  )

  case class B(
      numA: Int,
      numB: Int,
      strA: String,
      strB: String,
      strC: Option[String],
      strD: Option[String],
      strE: Option[String],
      strF: String,
      strG: String
  )

  case class C(
      a: Int,
      b: String,
      c: Int,
      d: String
  )

  def left(fld: String,
           lbl: String,
           f: ValidationFailure): Either[Seq[EF], Nothing] =
    Left(Seq(ExtractionFailure(fld, lbl, f)))

  def fail(f: ValidationFailure): Try[Nothing] =
    util.Failure(ExtractionException(ExtractionFailure("", "", f)))

  def succ[T](v: T): Try[T] =
    util.Success(v)

  def txt(in: String, cs: Constraint[String, TextField]*) =
    Try(v(in) << text(cs: _*) << reqNow)

  def num(in: String, cs: Constraint[Int, NumberField]*) =
    Try(v(in) << number(cs: _*) << reqNow)

  "DSL" should "produce values according to token" in {
    val in1 = Map(
      "a" -> "strA",
      "b" -> "strB",
      "c" -> "strC"
    )

    val in2 = in1 - "b" - "c"
    val in3 = in1 - "a"

    def t(m: MAP) =
      fromMap(m) { implicit ds =>
        A("a" << text << req, "b" << text << default("foo"), "c" << text << opt)
      }.toEither

    t(in1) shouldBe Right(A("strA", "strB", Some("strC")))
    t(in2) shouldBe Right(A("strA", "foo", None))
    t(in3) shouldBe left("a", "", MissingField)
  }

  it should "return all errors occurred" in {

    val res = result { implicit __ =>
      C(
        v("qwe", "c") << number << req,
        v("aa", "a") << text(minLn(3)) << req,
        v("123", "d") << number << req,
        v("aaa", "b") << text(minLn(3)) << req
      )
    }

    res.errors shouldBe List(
      ExtractionFailure("c", "", NumberField.ParseError),
      ExtractionFailure("a", "", MinLength.TooShort(2, 3))
    )
  }

  "FailureMapper" should "map occurred errors" in {

    sealed trait Res[+T]
    case class Succ[T](s: T) extends Res[T]
    case class Nope[T](n: T) extends Res[T]

    implicit val fm: FailureMapper[String] = FailureMapper {
      case "a" Field MissingField  => "a missing"
      case _ Both NonEmpty.IsEmpty => "smth empty"
    }

    val res = result { implicit __ =>
      val a  = valueNE("", "a") << text << req
      val b  = valueNE("", "b", "lb") << text << req
      val c  = v(" ", "foo") << text(nonEmpty) << opt
      val d  = v("heh") << number << default(0)
      val e  = v("a", "e", "num") << number << req
      val f  = v("a", "f", "num") << number << req
      val ff = v("a", "f") << number << req
      "heh mda" :: Nil
    }

    val re1 = res.get(
      Succ.apply,
      mapFailure
        .preMap(e => s"fm $e")
        .withFallback {
          case "lb" Label MissingField                  => "fb lb missing"
          case ("f", "num") Both NumberField.ParseError => "fb parse"
          case _                                        => "fb nope"
        }
        .mapEach
        .andThen(Nope.apply)
    )

    val rf1 = Seq(
      "fm a missing",
      "fb lb missing",
      "fm smth empty",
      "fb nope",
      "fb nope",
      "fb parse",
      "fb nope"
    )

    re1 shouldBe Nope(rf1)

  }

  "NE sources" should "treat empty input values as missing field" in {

    val in1 = Map(
      "nA" -> "",
      "nB" -> "10",
      "sA" -> "",
      "sB" -> "b",
      "sC" -> "",
      "sD" -> "d"
    )

    fromMap(in1) { implicit ds =>
      B(
        numA = fieldNE("nA") << number(min(5)) << default(1),
        numB = fieldNE("nB") << number(min(5)) << default(6),
        strA = fieldNE("sA") << text << default("_a"),
        strB = fieldNE("sB") << text << default("_b"),
        strC = fieldNE("sC") << text << opt,
        strD = fieldNE("sD") << text << opt,
        strE = fieldNE("sE") << text << opt,
        strF = fieldNE("sF") << text << default("_f"),
        strG = valueNE("gg") << text << default("_g")
      )
    }.getUnsafe shouldBe B(
      numA = 1,
      numB = 10,
      strA = "_a",
      strB = "b",
      strC = None,
      strD = Some("d"),
      strE = None,
      strF = "_f",
      strG = "gg"
    )
  }

  "Text field" should "handle constraints" in {

    (v("abc") << text(minLn(3)) << reqNow) shouldBe "abc"

    Try(v("ab") << text(minLn(3)) << reqNow) shouldBe fail(
      MinLength.TooShort(2, 3)
    )

    (v("abcd") << text(maxLn(4)) << reqNow) shouldBe "abcd"

    Try(v("abcde") << text(maxLn(4)) << reqNow) shouldBe fail(
      MaxLength.TooLong(5, 4)
    )

    (v("abcd") << text(lnBetween(2, 5)) << reqNow) shouldBe "abcd"

    Try(v("a") << text(lnBetween(2, 5)) << reqNow) shouldBe fail(
      LengthBetween.LengthNotInBounds(1, 2, 5)
    )

    Try(v("abcdef") << text(lnBetween(2, 5)) << reqNow) shouldBe fail(
      LengthBetween.LengthNotInBounds(6, 2, 5)
    )
  }

  "Number field" should "parse input and handle constraints" in {

    num("1337") shouldBe succ(1337)
    num("foo") shouldBe fail(
      NumberField.ParseError
    )

    num("5", min(5)) shouldBe succ(5)
    num("4", min(5)) shouldBe fail(
      Min.LessThanMin(4, 5)
    )

    num("6", max(8)) shouldBe succ(6)
    num("9", max(8)) shouldBe fail(
      Max.MoreThanMax(9, 8)
    )

    num("5", numBetween(1, 6)) shouldBe succ(5)
    num("0", numBetween(1, 6)) shouldBe fail(
      Between.NotInBounds(0, 1, 6)
    )
    num("7", numBetween(1, 6)) shouldBe fail(
      Between.NotInBounds(7, 1, 6)
    )

  }

  "(Not)OneOf constraints" should "work for different fields" in {
    val txtC  = va.oneOf("foo", "bar")
    val txtNC = va.notOneOf("foo", "bar")
    val numC  = va.oneOf(1337)
    val numNC = va.notOneOf(1337)

    txt("bar", txtC) shouldBe succ("bar")
    txt("heh", txtNC) shouldBe succ("heh")

    txt("bar", txtNC) shouldBe fail(
      NotOneOf.IsInSet("bar", txtNC.set)
    )
    txt("heh", txtC) shouldBe fail(
      OneOf.NotInSet("heh", txtC.set)
    )

    num("1337", numC) shouldBe succ(1337)
    num("0000", numNC) shouldBe succ(0)

    num("1337", numNC) shouldBe fail(
      NotOneOf.IsInSet(1337, numNC.set)
    )
    num("4", numC) shouldBe fail(
      OneOf.NotInSet(4, numC.set)
    )

    assertTypeError("num(\"1\", txtC)")
    assertTypeError("num(\"1\", txtNC)")
    assertTypeError("txt(\"foo\", numC)")
    assertTypeError("txt(\"foo\", numNC)")
  }

  "Pattern constraint" should "match full string" in {
    val patA = pattern(".w.")
    val patB = pattern("^.w.$")

    txt("UwU", patA) shouldBe succ("UwU")
    txt("UwU", patB) shouldBe succ("UwU")

    txt("UWU", patB) shouldBe fail(
      Pattern.NotMatched("UWU", patB.rawPat)
    )
    txt("UwUU", patA) shouldBe fail(
      Pattern.NotMatched("UwUU", patA.rawPat)
    )
    txt("UUwU", patA) shouldBe fail(
      Pattern.NotMatched("UUwU", patA.rawPat)
    )
  }

  "IsEmail constraint" should "validate full string as email" in {
    def t[R](em: String)(f: String => R): R = f(em)

    val valid = (e: String) => txt(e, isEmail) shouldBe succ(e)
    val invalid = (e: String) =>
      txt(e, isEmail) shouldBe fail(
        IsEmail.NotAValidEmail(e)
    )

    t("a@some.is")(valid)
    t("12gg@airmail.cc")(valid)

    t("a@some.local")(invalid)
    t("a@localhost")(invalid)
    t("a@tld")(invalid)

    t("a@some.nope")(invalid)
    t("sdasd")(invalid)
    t("a@@some.com")(invalid)
  }

  "IsEmpty constraint" should "treat whitespace string as empty if forbidden" in {
    val failC = fail(NonEmpty.IsEmpty)

    def ws(a: String) = Try(v(a) << text(nonEmpty) << reqNow)
    def wa(a: String) = Try(v(a) << text(nonEmptyAllowWS) << reqNow)

    def both(xs: String*): Unit = xs.foreach { x =>
      ws(x) shouldBe succ(x)
      wa(x) shouldBe succ(x)
    }

    val a = ""
    val b = "   "
    val c = "d  "
    val d = "  f"
    val e = " s  "
    val f = "\t"
    val g = "\tas"

    ws(a) shouldBe failC
    wa(a) shouldBe failC

    ws(b) shouldBe failC
    wa(b) shouldBe succ(b)

    ws(f) shouldBe failC
    wa(f) shouldBe succ(f)

    both(c, d, e, g)
  }

  "Boolean constraint" should "validate boolean conditions for different fields" in {
    /*_*/
    txt("foo", is(_.nonEmpty)) shouldBe succ("foo")
    txt("", isNot(_.nonEmpty)) shouldBe succ("")

    num("10", is(_ % 2 == 0)) shouldBe succ(10)
    num("11", is(_ % 2 == 0)) shouldBe fail(
      Bool.NotValidated(11)
    )
    /*_*/ // disables idea type inspection (idea bug)

    val cnd = condition[String, TextField](_.isEmpty)
    txt("", cnd) shouldBe succ("")
    txt("a", cnd) shouldBe fail(
      Bool.NotValidated("a")
    )
  }

}
