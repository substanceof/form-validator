name := "form-validator"
version := "0.1"
scalaVersion := "2.12.8"

libraryDependencies ++= Seq(
  "commons-validator" % "commons-validator" % "1.6",
  "org.scalatest"     %% "scalatest"        % "3.0.5" % Test
)
